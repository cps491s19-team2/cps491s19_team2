University of Dayton

Department of Computer Science

CPS 490 - Fall 2018

Dr. Phu Phung


## Capstone II Proposal


# Project Topic

Property Sketch Program

# Team members



1.  Daniel Allman, allmand2@udayton.edu
2.  Anna Duricy, duricya1@udayton.edu
3.  Christopher Pydych, pydychc1@udayton.edu


# Company Mentors

Dwayne Nickels, architect

Tyler Technologies

1, Tyler Way, Moraine, OH 45439


# Overview

Users will be able to access the property sketch program from their laptop or tablet. They will be able to do everything they need to do in the property sketch applet alone.

![Overview Architecture](property-sketch-overview.png "An Overview Architecture of property sketch")

Figure 1. - An Overview Architecture of the property sketch.

# Project Context and Scope

This project is going to serve the Appraisal and Tax organization of Tyler Technologies.

Counties will use this to sketch the outlines of buildings in their area and record information to value the properties.

Our team will advance the currently existing property sketching technology to be more modern and have more and improved functionalities.


# High-level Requirements

Sketch application can read sketch files from an in-memory data structure.  The format of the sketch files will need to be determined.

Sketch application can write representations of sketches to an in-memory data structure.

There will need to be some way for a human to “load” the in-memory data structure with a sketch file.

Sketch application will automatically zoom to a zoom level that allows all polygons to be viewable on the canvas when a sketch is first rendered.

Sketch application can read a pre-existing sketch vector and render the polygon with correct dimensions and in correct position

Users can zoom in/out.

Users can pan around the sketch canvas.

Users can edit an existing polygon and change the dimensions and placement of the polygon

Users can create new polygons

The origin point of any new polygon can be anywhere on the canvas

Users can draw polygons of n sides

By default, polygons are drawn with 90 deg. angles and all lines are “straight” (ie. it’s not free-form mouse draw)

Users can draw polygons with non-90 deg. angled lines (ie. diagonal lines) and curves (ie. “bows” in iSketch terminology)

Polygons must “finish” in order to be accepted/persisted

On completing a polygon, the sketch application will calculate an area and perimeter for the polygon

Users can assign one and only one “code” to a polygon; users do not have to assign a “code”

The sketch application opens natively in a browser and requires no additional plug-ins, apps, etc. to be downloaded on a client’s workstation

A nice-to-have feature would be an “auto-finish” option to automatically close an open polygon (iSketch has this, I believe)



# Technology

This will be a Java web app with an Oracle backend that runs as an applet in the browser.

We will use HTML-5 to develop this application.

Tyler Technologies has an existing version of the property sketch application, but it has not been updated for many years.


# Project Management

Bitbucket link: https://bitbucket.org/cps491-scrum-team2/property-sketch-repo/src/master/

Trello board link: https://trello.com/b/RienJTbg/trello-board


# Company Support

We will meet with to the company every two weeks. Half of the meetings will be virtual, and half will be on location.
