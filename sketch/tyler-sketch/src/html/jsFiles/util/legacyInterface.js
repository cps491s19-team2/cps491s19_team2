function drawRectObj(string){
    var [width, height] = string.split('x');
    width = parseInt(width);
    height = parseInt(height);
    //alert("Height: " + height + " width " + width)
    var startPoint = globalStateData.getCommonShapeStartPoint();

    var newRect = new shapeObj(startPoint);
    // alert("Start point: " + JSON.stringify(startPoint))
    // drawline(startPoint.x + width, startPoint.y);
    // drawline(startPoint.x + width, startPoint.y + height);
    // drawline(startPoint.x, startPoint.y + height);
    newRect.addStraightLine(new pointObj(startPoint.x + width, startPoint.y));
    newRect.addStraightLine(new pointObj(startPoint.x + width, startPoint.y + height));
    newRect.addStraightLine(new pointObj(startPoint.x, startPoint.y + height));
    //newRect.addStraightLine(new pointObj(startPoint.x, startPoint.y));
    newRect.addLastLine();

    globalStateData.addShape(newRect);
    // We need to redraw all the shapes in order for this be drawn immediately
    drawShapeObjs();
}

// draws a triangle with specific height and length
function drawTriObj(string){
    var [height, width] = string.split('x');
    height = parseInt(height);
    width = parseInt(width);
    var startPoint = globalStateData.getCommonShapeStartPoint();

    var newTri = new shapeObj(startPoint);
    newTri.addStraightLine(new pointObj(startPoint.x + width / 2, startPoint.y - height));
    newTri.addStraightLine(new pointObj(startPoint.x + width, startPoint.y));
    //newTri.addStraightLine(new pointObj(startPoint.x, startPoint.y));
    newTri.addLastLine();

    globalStateData.addShape(newTri);

    // We need to redraw all the shapes in order for this be drawn immediately
    drawShapeObjs();
}

// draws a circle
function drawCircleObj(string){
    // alert(string)
    var radius = parseInt(string);

    // alert(radius)
    var startPoint = globalStateData.getCommonShapeStartPoint();
    // alert("After get startpoint")
    // alert("Getpoint(0): " + getPoint(0))
    //alert("startpoint" + JSON.stringify(startPoint))
    // alert("After state start point")
    var newCirc = new shapeObj(startPoint);
    newCirc.addCurvedLine(
        new pointObj(radius * 2, startPoint.y),
        new pointObj(startPoint.x, startPoint.y + radius),
        new pointObj(startPoint.x + (radius * 2), startPoint.y + radius)
    );
    newCirc.addCurvedLine(
        new pointObj(radius * 2, startPoint.y),
        new pointObj(startPoint.x, startPoint.y - radius),
        new pointObj(startPoint.x + (radius * 2), startPoint.y - radius)
    );

    globalStateData.addShape(newCirc);

    // We need to redraw all the shapes in order for this be drawn immediately
    drawShapeObjs();

}

function parse(vectorString){
    var startingPoint = false;
    // for loading in correct beginning position
    indexOfColon = vectorString.indexOf(':');
    if(indexOfColon != -1) {
        startingPoint = true;
        var start = vectorString.split(':')[0];
        vectorString = vectorString.split(':')[1];
        var x = snapToGrid(start.split(',')[0], globalStateData.getXOffset());
        var y = snapToGrid(start.split(',')[1], globalStateData.getYOffset());
        var startPoint = new pointObj(x, y);
        globalStateData.setCommonShapeStartPoint(startPoint);
        // var [first, vectorString] = vectorString.split(':');
        // var [first, second] = first.split(',');
    }
    //vectorString = vectorString.substring(vectorString.indexOf(':')+1);
    //alert(first + "\n" + second + "\n" + vectorString);
    var parsedShapes = vectorString.split(';');
    //alert(parsedShapes.length);
    for(let i = 0; i < parsedShapes.length; i++){

        var singleString = parsedShapes[i].split(',');

        //alert(singleString);

        // var curPoint;

        curCoords = globalStateData.getCommonShapeStartPoint();

        // curCoords.x;
        // curCoords.y;
        //alert("curCoords " + curCoords[0] + " " + curCoords[1]);
        var shape = globalStateData.getCurrentWorkingShape();

        for(let j = 0; j < singleString.length; j++){
            var cmd = singleString[j].trim();

            // if(j != 0) {
            //     curCoords = shapesArray[shapesCount][pointCount - 1];
            //
            //     x = curCoords[0];
            //     y = curCoords[1];
            // }
            //alert("curCoords " + x + " " + y);

            //alert(cmd + " " + cmd.charAt(0) + " " + cmd.charAt(1));

            var dist = parseInt(cmd.slice(1, cmd.length)) * globalStateData.gridLength;
            //alert(dist);
            //alert("first " + first + " second " + second);
            // ctx.moveTo(first, second);

            switch (cmd.charAt(0)) {
                case 'u' :
                case 'U' :
                    shape.addStraightLine(new pointObj(curCoords.x, (curCoords.y - dist)));
                    curCoords.y -= dist;
                    break;
                case 'd' :
                case 'D' :
                    shape.addStraightLine(new pointObj(curCoords.x, (curCoords.y + dist)));
                    curCoords.y += dist;
                    break;
                case 'l' :
                case 'L' :
                    shape.addStraightLine(new pointObj((curCoords.x - dist), curCoords.y));
                    curCoords.x -= dist;
                    break;
                case 'r' :
                case 'R' :
                    shape.addStraightLine(new pointObj((curCoords.x + dist), curCoords.y));
                    curCoords.x += dist;
                    break;
                default:
                    alert("You done goofed");
            }
        }

        shape.addLastLine();

        globalStateData.addShape(shape);

        drawShapeObjs();
    }
    drawShapeObjs();
}


function saveModel(){
    var format = "<!>";
    $("input").each(function() {
        if ($(this).hasClass("save-radio")) {
            if (this.checked) {
                format = this.getAttribute("value");
            }
        }
    });
    if (format === "<!>"){
        alert("Please select a save type");
        return;
    }
    alert(format);
    switch (format){
        case 'vector':
            //alert("print the list of shapes in a vector string...lots of conditional evaulation")
            break;
        case 'json':
            //alert("give json representation of the shapes in the console")
            for(let i = 0; i < globalStateData.getNumShapes(); i++){
                console.log(JSON.stringify(globalStateData.getShape(i)));
            }
            break;
        case 'other':
            //alert("some other third thing");
            break;
        default:
            alert('something is not right');

    }
}
