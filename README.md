University of Dayton
Department of Computer Science
CPS 491 - Capstone II, Spring 2019
Instructor: Dr. Phu Phung

# Capstone II Project

## Property Sketch HTML5 Implementation

## Team Members

1. Anna Duricy, duricya1@udayton.edu
2. Dan Allman, allmand2@udayton.edu
3. Chris Pydych, pydychc1@udayton.edu

## Company Mentors

1.Dwayne Nickels
	Tyler Technologies
	1, Tyler Way, Moraine, OH 45439

## Project Management Information

Management board: https://trello.com/b/fMsBLCJF/our-team-trello-board
Source code repository: https://bitbucket.org/cps491-scrum-team2/property-sketch-repo/
Project homepage: https://cps491s19-team2.bitbucket.io/

## Revision History


Date             Version          Description
-----------------------------------------------
1/28/2019        0.0              Sprint 0
-----------------------------------------------
2/13/2019		 1.0			  Sprint 1
-----------------------------------------------
3/5/2019	 	 2.0	 		  Sprint 2
-----------------------------------------------
4/2/2019	 	 3.0	 		  Sprint 3
-----------------------------------------------
4/25/2019		 4.0			  Sprint 4
-----------------------------------------------

## Overview

Users will be able to access the property sketch program from their laptop or tablet. They will be able to do everything they need to do in the property sketch applet alone.

![Overview Architecture](images/property-sketch-overview.png "Overview architecture")

Figure 1. - An Overview Architecture of the property sketch.



## Project Context and Scope

This project is going to serve the Appraisal and Tax organization of Tyler Technologies.

Counties will use this to sketch the outlines of buildings in their area and record information to value the properties.

Our team will advance the currently existing property sketching technology to be more modern and have more and improved functionalities.

## System Analysis

### High-level Requirements

Sketch application can read sketch files from an in-memory data structure. The format of the sketch files will need to be determined.

Sketch application can write representations of sketches to an in-memory data structure.

There will need to be some way for a human to “load” the in-memory data structure with a sketch file.

Sketch application will automatically zoom to a zoom level that allows all polygons to be viewable on the canvas when a sketch is first rendered.

Sketch application can read a pre-existing sketch vector and render the polygon with correct dimensions and in correct position

Users can zoom in/out.

Users can pan around the sketch canvas.

Users can edit an existing polygon and change the dimensions and placement of the polygon

Users can create new polygons

The origin point of any new polygon can be anywhere on the canvas

Users can draw polygons of n sides

By default, polygons are drawn with 90 deg. angles and all lines are “straight” (ie. it’s not free-form mouse draw)

Users can draw polygons with non-90 deg. angled lines (ie. diagonal lines) and curves (ie. “bows” in iSketch terminology)

Polygons must “finish” in order to be accepted/persisted

On completing a polygon, the sketch application will calculate an area and perimeter for the polygon

Users can assign one and only one “code” to a polygon; users do not have to assign a “code”

The sketch application opens natively in a browser and requires no additional plug-ins, apps, etc. to be downloaded on a client’s workstation

A nice-to-have feature would be an “auto-finish” option to automatically close an open polygon (iSketch has this, I believe)


### Overview Use Case Diagram

![Use Case Diagram](images/useCaseDiagram.png "Use Case Diagram")

#### Use Case Descriptions

**Login**

User can login to the property sketch system to access their own account, possibly using TylerTech's credential system. This also fetched the users work.

**Logout**

User can log out of the property sketch system, this will will end the users session and not send any unsaved changes to the server.

**Draw shapes**

User can draw shapes to represent the properties they observe. They will be able to draw both with shape based drawing or with
the input strings.

Note: the legacy input system will be translated to shape drawings

**Edit shapes**

User will be able to edit the shapes that they draw through shape drawing or the input string.

Note: the legacy input system will be translated to shape drawings

**Write description of property**

User will be able to write a text description of the property to remark on any notable features.

**Pan and zoom**

User will be able to pan and zoom to view the sketch as they please.

**Import and export**

User will be able to import and export sketches to and from the system.

## System Design

### Use-Case Realization

#### Calculate area
![Calculate area](images/Sprint3CalculateArea.png "Calculate area")

#### Add and change label
![Add label](images/Sprint3LabelShapes.png "Add label")

![Change label](images/Sprint3ChangeLabel.png "Change label")

#### Draw rectangles and triangles with specific dimensions
![Enter input for rectangle](images/Sprint3DrawRect.png "Enter input for rectangle")

![Enter input for triangle](images/Sprint3DrawTri.png "Enter input for triangle")

![Draw triangle and rectangle](images/Sprint3DrawRectAndTri.png "Draw rectangle and triangle")

#### Draw circles and curves
![Curves and circles](images/Sprint3Curves.png "Curves and circles")

### Database

No database needed.

### User Interface

## Implementation

### serverSideNode.js

	var fs = require('fs');
	var path = require('path');
	var express = require('express');
	var app = express();

	var htmlPath = path.join(__dirname, 'html');

	app.use('/sketch', express.static(htmlPath));

	var server = app.listen(3000, function () {
		var host = 'localhost';
		var port = server.address().port;
		console.log('listening on http://' + host + ':' + port + '/');
	});

This is the basic code to run a website from node.js, 

### Calculating Area

	function calcAreas(){

		var i = 0;
		for(i; i < shapesCount; i++){
			let pointCount = shapesArray[i].length;
			var j, sum = 0, perim = 0;
			if(!(metaArray[i].hasCurves)){
				for(j = 0; j < pointCount; j++){
					sum +=      shapesArray[i][(j) % pointCount][0] * shapesArray[i][(j + 1) % pointCount][1]
								- shapesArray[i][(j) % pointCount][1] * shapesArray[i][(j + 1) % pointCount][0];
					perim +=    hypotenuse(shapesArray[i][(j + 1) % pointCount][0] - shapesArray[i][(j) % pointCount][0],
											shapesArray[i][(j + 1) % pointCount][1] - shapesArray[i][(j) % pointCount][1]);
				}
				sum = sum / 2;
				sum = Math.abs(sum);
				sum = pixelsSquaredToGrid(sum, gridLength * zoomDeg);
				metaArray[i].setTotalArea(sum);
				metaArray[i].setPerimeter(perim);
				console.log(sum);
				console.log(perim);
			}
		}
	}


This is the code that finds the area of the drawn figure. Simple math really, it takes the length of each line and the points and uses a math algorithm to figure out the area.

### Draw rectangle with specific dimensions

	const rectObj = function(xCoord, yCoord, width, height){
		this.x = xCoord;
		this.y = yCoord;
		this.w = width;
		this.h = height;
	}

This code is just an example of what the construct rectObj is made of. It gets the x-coordinate, y-coordinate, width and height from the user and returns the rectangle.

### Adding a label to each polygon

	function addLabel(shape, label) {

		// add the label to the metaArray information
		metaArray[shape].name = label + shape;

		// find the top left coordinate to attach the label to
		var coords = findTopLeftCoordinate(shape);
		var x = coords[0];
		var y = coords[1];

		// creates a white rectangle the length of the label for easier readability
		ctx.fillStyle = "#FFFFFF";
		ctx.fillRect(x, y-15, label.length * 9, 14);

		// prints the label onto the screen
		ctx.fillStyle = "#000000";
		ctx.font = labelFontSize + "px Courier New";
		ctx.fillText(label, x, y-4);
	}

This code draws a white rectangle at the correct coordinates and then prints the label
on top of the rectangle.


### Change label

	function changeLabel(string) {

	// parse input string ("0:Name", for example) into shape number and label
	var [shape, label] = string.split(';');
	addLabel(shape, label);
	}

This code receives an input string specifying the shape number to change and the desired
label, parses this string, then calls addLabel to set the new label.

### Draw rectangle with specific dimensions

	function drawRectangle(string){

		// parse the input string ("20x20", for example) into desired width and height
		var [width, height] = string.split('x');
		width = parseInt(width);
		height = parseInt(height);

		// find the starting x and y coordinates of the shape
		x = shapesArray[shapesCount][pointCount-1][0];
		y = shapesArray[shapesCount][pointCount-1][1];

		// draw the lines to form the square
		drawLine(x+width, y);
		drawLine(x+width, y + height);
		drawLine(x, y + height);
		completeShape();
	}

This code receives an input string, parses it to find the desired dimensions, and then
draws a rectangle with those dimensions from the point specified by the user.

### Draw triangle with specific dimensions

	function drawTriangle(string){

		// parse the input string ("20x20", for example) into desired width and height
		var [height, width] = string.split('x');
		height = parseInt(height);
		width = parseInt(width);

		// find the starting x and y coordinates of the triangle, specified
		// by user clicking on grid before clicking "Draw triangle"
		x = shapesArray[shapesCount][pointCount-1][0];
		y = shapesArray[shapesCount][pointCount-1][1];

		// draw the lines to form the triangle
		drawLine(x+(width/2), y+height);
		drawLine(x-(width/2), y + height);
		completeShape();
		//drawLine()
	}

This code receives an input string, parses it to find the desired dimensions, and then
draws a triangle with those dimensions from the point specified by the user.

### Curve Line Drawing

	this.addCurvedLine = function(point, c1, c2){
		this.lineList[this.numLines] = new lineObj(this.pointList[this.numPoints], point, true, c1, c2);
		this.numPoints++;
		this.numLines++;
		this.pointList[this.numPoints] = point;
	}

This is how we add a curved line. This takes the numLines argument and passes it to a point list which allows the curve to be displayed.

### Pixels to Length Conversion

	function pixelsSquaredToGrid(numPixels, gridSide){
		return numPixels / (gridSide * gridSide);
	}

	function squareGridToUnits(numSquares, ratio){
		return numSquares * ratio;
	}

	function rgbToHex(r, g, b) {
		return "#" + ((r << 16) | (g << 8) | b).toString(16);
	}

This is the pixel to any length conversion. This can be canged to meet any specific requirements that the user would want.

### Pan

	$("#myCanvas").mousemove(
			function(ev){
				var x = ev.clientX - canvas.offsetLeft;
				var y = ev.clientY - canvas.offsetTop;

				// other code
				
				else if(globalStateData.getPanMode() && globalStateData.getMouseDown()){
					globalStateData.setXOffset(globalStateData.getXOffset() + globalStateData.getTempX() - x);
					globalStateData.setYOffset(globalStateData.getYOffset() + globalStateData.getTempY() - y);

					globalStateData.setTempX(x);
					globalStateData.setTempY(y);

					drawShapeObjs();
				}
				
This allows the user to pan around the canvas.

### Zoom
	
	// in globalState.js
	this.zoomIn = function(){this.zoomDeg += .1;}
    this.zoomOut = function(){ ((this.zoomDeg >= .2) ? this.zoomDeg -= .1 : this.zoomDeg = .1); }
    this.getZoomDeg = function(){ return this.zoomDeg; }
	
	// in index.html
	document.addEventListener("keypress",
			function myEventHandler(e){
				//alert(e.keyCode);
				var keyCode = e.keyCode;
				
				// z or Z (zoom in)
				if(keyCode == 122 | keyCode == 90){
					//zoom(true);
					globalStateData.zoomIn();
					drawShapeObjs();


				}
				// x or X (zoom out)
				if(keyCode == 120 | keyCode == 88){
					//zoom(false);
					if(globalStateData.getZoomDeg() > .7) {
						globalStateData.zoomOut();
						drawShapeObjs();
					}

This allows the user to zoom in and out, but not zoom out too far.

### Auto-zoom

	$("#autoZoom").click(function(){
		var height = globalStateData.getHeight();
		var width = globalStateData.getWidth();
		var top = globalStateData.getTopCoordinate();
		var bottom = globalStateData.getBottomCoordinate();
		var rightmost = globalStateData.getRightmostCoordinate();
		var leftmost = globalStateData.getLeftmostCoordinate();
		var zoomDeg = globalStateData.getZoomDeg();


		if(1 / (globalStateData.getZoomDeg()+.1) < top && height / (globalStateData.getZoomDeg()+.1) > bottom && 1 / (globalStateData.getZoomDeg()+.1) < leftmost && width / (globalStateData.getZoomDeg()+.1) > rightmost) {
			while(1 / (globalStateData.getZoomDeg()+.1) < top && height / (globalStateData.getZoomDeg()+.1) > bottom && 1 / (globalStateData.getZoomDeg()+.1) < leftmost && width / (globalStateData.getZoomDeg()+.1) > rightmost) {
				//alert(1 / globalStateData.getZoomDeg() + " < " +  top + " " + height / globalStateData.getZoomDeg() + " > " + bottom + " " + 1 / globalStateData.getZoomDeg() + " < " + leftmost + " " + width / globalStateData.getZoomDeg() + " > " + rightmost);
				globalStateData.zoomIn();
				drawShapeObjs();
			}
		}
		else if(1 * (globalStateData.getZoomDeg()-.1) < top && height * (globalStateData.getZoomDeg()-.1) > bottom && 1 * (globalStateData.getZoomDeg()-.1) < leftmost && width * (globalStateData.getZoomDeg()-.1) > rightmost) {
			while(1 * (globalStateData.getZoomDeg()-.1) < top && height * (globalStateData.getZoomDeg()-.1) > bottom && 1 * (globalStateData.getZoomDeg()-.1) < leftmost && width * (globalStateData.getZoomDeg()-.1) > rightmost) {
				//alert(1 / globalStateData.getZoomDeg() + " < " +  top + " " + height / globalStateData.getZoomDeg() + " > " + bottom + " " + 1 / globalStateData.getZoomDeg() + " < " + leftmost + " " + width / globalStateData.getZoomDeg() + " > " + rightmost);
				globalStateData.zoomOut();
				drawShapeObjs();
			}
		}
		
This will automatically zoom the canvas to nicely fit all the shapes that are currently drawn.

### Translate

	$("#myCanvas").mousedown(
			function(ev){

				var x = ev.clientX - canvas.offsetLeft;
				var y = ev.clientY - canvas.offsetTop;

				var pixelData = ctxHolder.getContext().getImageData(x, y, 1, 1).data;

				if(globalStateData.getTranslateMode()){
					// alert(pixelData[0] + ", " + pixelData[1] + ", " + pixelData[2] + ", " + pixelData[3]);
					if(isBackground(pixelData)){ return; }

					var clickHex = rgbToHex(pixelData[0], pixelData[1], pixelData[2]);

					for(let i = 0; i < globalStateData.getNumShapes(); i++){
						// alert(clickHex + " " + globalStateData.getShape(i).metadata.getColor());
						if(clickHex === globalStateData.getShape(i).metadata.getColor()){
							globalStateData.setSelectedShape(globalStateData.getShape(i));
							setTableSelection(i);
							globalStateData.setTempX(x);
							globalStateData.setTempY(y);
						}
					}
				}
				
This allows you to change the placement of shapes that have been drawn.

### Sprint Cycle	

**Sprint 0**
-Technology preparation
-Documentation

**Sprint 1**
-Basic Framework
-Using OpenJSCAD to draw
-More Documentation

**Sprint 2**
-HTML Canvas

**Sprint 3**
-Arithmetic and Logic
-Finishing up the drawing feature

**Sprint 4**
-Pan/Zoom
-Read/Write files


### Development Approach

We will write it using JavaScript and HTML.

The property sketch system will be a larger part of Tyler Technology's existing iasWorld program. However, we are simply
creating an HTML5 application to demonstrate the capabilities of our project. It will be imported into their larger system
by the company after we have finished.

## Software Process Management

### Scrum

Our team meets three times a week, twice during class and once outside of class on Mondays at 7. We divide up the work at
these meetings and finish our tasks both during these meetings and outside of them if necessary. We mostly collaborate with
each other through texting and Discord. We collaborate with the company through emails.

### Trello board
![Trello Board](images/trelloBoard.png "Trello Board")

Sprint 1: We are making the basic frontend of our program. We will also begin to implement the logic and arithmetic needed
in the program.
Sprint 2: Import and export data and drawing shapes using both the legacy input string and free drawing.
Sprint 3: Improve the user interface and add interactivity with the interface, such as zooming and panning.
Sprint 4: Implement an odd variety of requirements, such as reading and writing to files, reading preexisting sketches,
			assigning codes to polygons, and auto-finishing polygons.

![Gantt Chart](images/gantt.png "Gantt Chart")

## Scrum Process

### Sprint 0

Duration: 1/15/2019 - 1/28/2019

### Completed Tasks:

Task 1: Bitbucket setup
Task 2: Project proposal and report
			a. Overview use case diagram
			b. Use case descriptions
Task 3: Trello board maintenance
Task 4: Tool/software installation

### Contributions:

1. Anna Duricy, 11 hours, contributed in Bitbucket setup, project report, use case diagram, use case descriptions, Trello maintenance, gantt chart
2. Dan Allman, 10 hours, contributed in Bitbucket setup and programming research
3. Chris Pydych, 10 hours, contributed in readme and Bitbucket setup, Trello maintenance, use case diagrams

### Sprint Retrospection

We had very good teamwork and discussions at our team meetings. We know what is expected of us from the company and have
a solid plan for how we are going to accomplish our goals. I don't think that there is much that needs improving from this
sprint because everything went very well.

### Sprint 1

Duration 1/29/2019 - 2/13/2019

### Completed Tasks:

Task 1: Basic Framework
			a. Zoom
			b. Pan
			c. Input/Output
			d. Draw
Task 2: Hosting Website

### Contributions:

1. Anna Duricy, 11 hours, contributed in readme and Sprint 1 powerpoint
2. Dan Allman, 10 hours, contributed in programming research and code
3. Chris Pydych, 9 hours, contributed in readme and Sprint 1 powerpoint

## Sprint Retrospection

We accomplished a lot in our meetings. We figured out the library that we will use in our project. Our project is coming along 
well and we have the basic framework in our project complete. I do not believe there is much improvement needed as we have set
the goals of Sprint 2 and can achieve them.

### Sprint 2

Duration 2/13/2019 - 3/4/2019

### Completed Tasks:

Task 1: More Framework
			a. Read external data
			b. Legacy input string
			c. Draw
Task 2: Hosting Website
			a. Update web page with Markdown and video

### Contributions:

1. Anna Duricy, 13 hours, contributed in programming (legacy input box, IO, zoom, auto-connect points, draw), update web page, video,  Sprint 2 powerpoint
2. Dan Allman, 10 hours, contributed in programming (legacy input box, file input, IO, CSS and HTML)
3. Chris Pydych, 11 hours, contributed in programming (legacy input box, file input), readme, web page, Trello maintenance

## Sprint Retrospection

We accomplished a lot in our meetings. We figured out the library that we will use in our project. Our project is coming along 
well and we have the basic framework in our project complete. I do not believe there is much improvement needed as we have set
the goals of Sprint 3 and can achieve them.

### Sprint 3

Duration 3/4/2019 - 4/1/2019

### Completed Tasks:

Task 1: Arithmetic
			a. Calculate Area
			b. Perimeter
			c. Number of distinct structures
Task 2: Logic
			a. Generalized coordinates as abstract geometric structure
Task 3: Curves
			a. Draw a quadratic curve

### Contributions:

1. Anna Duricy, 24 hours, contributed in programming (Codes for Polygons), video,  Sprint 3 powerpoint
2. Dan Allman, 20 hours, contributed in programming (Calculating Area, Perimeter), generally cleaning up the spaghetti code
3. Chris Pydych, 21 hours, contributed in programming (Cuve, Arithmetic), readme, Trello maintenance

## Sprint Retrospection

We accomplished a lot. We do need to meet more often though as we should have been able to complete everything for sprint 3.
There were some minor setbacks, however we have discussed as a group on how to move past those. Sprint 4 will be completed and
our fiinal project for Tyler Technologies will be done.  

### Sprint 4

Duration 4/1/2019 - 4/25/2019

### Completed Tasks:

Task 1: Pan/Zoom
			
Task 2: Edit drawn shapes

Task 3: Auto-zoom

### Contributions:

1. Anna Duricy, 21 hours, contributed in programming, video,  Sprint 3 powerpoint, Website
2. Dan Allman, 19 hours, contributed in programming, generally cleaning up the spaghetti code, redoing most of the code
3. Chris Pydych, 18 hours, contributed in programming, readme, Trello maintenance

## Sprint Retrospection

We have finished the Capstone Project. It was a very interesting and learnable experience. The group grew as a team and helped eachother along the way.


## Demo

https://www.youtube.com/embed/E7mza8GTY8U

## Acknowledgments

We would like to thank Tyler Technologies, especially Dwayne Nickels, for sponsoring this project and allowing us to work
with them and learn from them.

